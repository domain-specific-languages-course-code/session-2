package dsl2

interface IEvent
interface IState

class States {
    enum class Events : IEvent { ElectricOn, ElectricOff, Clap}
    enum class States : IState { NotPowered, Powered, LightsOn}

    companion object {
        var machine : StateMachine? = null
        var currentState : State? = null
        var currentEventId : IEvent? = null

        @JvmStatic
        fun main(args: Array<String>) {
            stateMachine()
                state(States.LightsOn)
                    on(Events.ElectricOff)
                        goto(States.NotPowered)
                    on(Events.Clap)
                        goto(States.Powered)
                state(States.Powered)
                    on(Events.ElectricOff)
                        goto(States.NotPowered)
                    on(Events.Clap)
                        goto(States.LightsOn)
                state(States.NotPowered)
                    on(Events.ElectricOn)
                        goto(States.Powered)
                startState(States.NotPowered)

            machine?.handle(Events.ElectricOn)
            machine?.handle(Events.ElectricOff)
            machine?.handle(Events.ElectricOn)
            machine?.handle(Events.Clap)
            machine?.handle(Events.Clap)
        }

        fun stateMachine() {
            machine = StateMachine()
        }
        fun state(stateId : IState) {
            currentState = State(stateId)
            machine!!.states[stateId] = currentState!!
        }
        fun on(eventId : IEvent) {
            currentEventId = eventId
        }
        fun goto(stateId : IState) {
            currentEventId?.let { event ->
                currentState?.let {state ->
                    state.transitions[event] = stateId
                } ?: throw IllegalStateException("You didn't define the current state before the on/goto")
            } ?: throw IllegalStateException("You didn't define the 'on' before the 'goto'")
        }
        fun startState(stateId : IState) {
            machine?.let {
                it.currentState = stateId
            } ?: throw IllegalStateException("You called startState() before stateMachine()")
        }
    }
}

class StateMachine {
    val states = mutableMapOf<IState, State>()
    var currentState : IState? = null
    fun handle(eventId : IEvent) {
        states[currentState]?.let { state ->
            state.transitions[eventId]?.let {nextState ->
                currentState = nextState

            }
            println("State changed to $currentState")
        }
    }
}

class State(val stateId : IState) {
    val transitions = mutableMapOf<IEvent, IState>()
}
